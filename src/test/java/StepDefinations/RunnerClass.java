package StepDefinations;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.CucumberOptions.SnippetType;

@CucumberOptions(features = {
		"/home/ajeez/eclipse-workspace/DemoWebShopAssignment/src/test/resources/DemoWebShop/DemoFeature.feature" },
      glue = "StepDefinations", snippets = SnippetType.CAMELCASE, monochrome = true)
public class RunnerClass extends AbstractTestNGCucumberTests {

}
